package com.syncadapterdemo;

import android.accounts.AccountManager;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * Created by root on 27/3/18.
 */

public class AuthenticatorService extends Service {


    private Authenticator authenticator;

    public AuthenticatorService() {
        super();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        IBinder ret = null;
        if (intent.getAction().equals(AccountManager.ACTION_AUTHENTICATOR_INTENT))
            ret = getAuthenticator().getIBinder();
        return ret;
    }

    public Authenticator getAuthenticator() {

        if (authenticator == null) {
            authenticator = new Authenticator(this);
        }
        return authenticator;
    }
}
