package com.syncadapterdemo;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Created by root on 27/3/18.
 */

public class MyIPContentProvider extends ContentProvider {


    public static final int IP_DATA = 1;
    private static final String AUTHORITY = "com.syncadapterdemo.provider";
    private static final String TABLE_IP_DATA = "ip_data";
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + '/' + TABLE_IP_DATA);
    private static final UriMatcher URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);
    private IpDataDBHelper myDB;

    static {
        URI_MATCHER.addURI(AUTHORITY, TABLE_IP_DATA, IP_DATA);
    }


    @Override
    public boolean onCreate() {
        myDB = new IpDataDBHelper(getContext(), null, null, 1);
        return false;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        int uriType = URI_MATCHER.match(uri);
        Cursor cursor = null;
        switch (uriType) {
            case IP_DATA:
                cursor = myDB.getAllIpData();
                break;
            default:
                throw new IllegalArgumentException("UNKNOWN URL");
        }
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        int uriType = URI_MATCHER.match(uri);
        long id = 0;
        switch (uriType) {
            case IP_DATA:
                id = myDB.AddIPData(values);
                break;
            default:
                throw new IllegalArgumentException("UNKNOWN URI :" + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return Uri.parse(values + "/" + id);
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        int uriType = URI_MATCHER.match(uri);
        int rowsDeleted = 0;
        switch (uriType) {
            case IP_DATA:
                rowsDeleted = myDB.deleteAllIpData();
                break;
            default:
                throw new IllegalArgumentException("UNKNOWN URI :" + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsDeleted;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }
}
