package com.syncadapterdemo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static android.provider.Contacts.SettingsColumns.KEY;

/**
 * Created by root on 27/3/18.
 */

public class IpDataDBHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "ip.db";
    public static final String TABLE_IP_DATA = "ip";

    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_IP = "ip";
    public static final String COLUMN_COUNTRY_CODE = "country_code";
    public static final String COLUMN_COUNTRY_NAME = "country_name";
    public static final String COLUMN_CITY = "city";
    public static final String COLUMN_LATITUDE = "latitude";
    public static final String COLUMN_LONGITUDE = "longitude";


    public IpDataDBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE " + TABLE_IP_DATA + "( " + COLUMN_ID + " INTEGER PRIMARY KEY ,"
                + COLUMN_IP + " INTEGER ," + COLUMN_COUNTRY_CODE + " INTEGER ," +
                COLUMN_COUNTRY_NAME +
                " TEXT ," + COLUMN_CITY + " TEXT ," + COLUMN_LATITUDE + " INTEGER ," +
                COLUMN_LONGITUDE + " INTEGER)";
        db.execSQL(CREATE_TABLE);
        Log.d("SQL", CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_IP_DATA);
        onCreate(db);
    }

    public long AddIPData(ContentValues values) {
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        long insertedRow = sqLiteDatabase.insert(TABLE_IP_DATA, null, values);
        return insertedRow;
    }

    public Cursor getAllIpData() {
        String[]
                projection = {COLUMN_ID, COLUMN_IP, COLUMN_COUNTRY_CODE, COLUMN_COUNTRY_NAME, COLUMN_CITY, COLUMN_LATITUDE
                , COLUMN_LONGITUDE};
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        Cursor cursor = sqLiteDatabase.query(TABLE_IP_DATA, projection, null, null, null, null, null);
        return cursor;
    }

    public int deleteAllIpData() {
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        int rowDeleted = sqLiteDatabase.delete(TABLE_IP_DATA, null, null);
        return rowDeleted;
    }
}
