package com.syncadapterdemo;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.CursorAdapter;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private static final String ACCOUNT_TYPE = "com.syncadapterdemo";
    private static final String AUTHORITY = "com.syncadapterdemo.provider";
    private static final String ACCOUNT_NAME = "Sync";


    CursorAdapter cursorAdapter;
    Account mAccount;
    private String TAG = this.getClass().getCanonicalName();

    public SharedPreferences mSharedPreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mSharedPreferences = getSharedPreferences("MyIp", 0);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        setUpSync();
    }

    private void setUpSync() {
        mAccount = createSyncAccount(this);
        Bundle settingBundle = new Bundle();
        settingBundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        settingBundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        ContentResolver.requestSync(mAccount, AUTHORITY, settingBundle);
        ContentResolver.setSyncAutomatically(mAccount, AUTHORITY, true);
        ContentResolver.addPeriodicSync(mAccount, AUTHORITY, Bundle.EMPTY, 60);

    }

    private Account createSyncAccount(MainActivity mainActivity) {
        Account account = new Account(ACCOUNT_NAME, ACCOUNT_TYPE);
        AccountManager
                accountManager = (AccountManager) mainActivity.getSystemService(ACCOUNT_SERVICE);
        if (accountManager.addAccountExplicitly(account, null, null)) {

        } else {
        }
        return account;
    }



    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(syncStaredReceiver, new IntentFilter(SyncAdapter.SYNC_STARTED));
        registerReceiver(syncFinishedReceiver, new IntentFilter(SyncAdapter.SYNC_FINISHED));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(syncStaredReceiver);
        unregisterReceiver(syncFinishedReceiver);
    }

    private BroadcastReceiver syncFinishedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "Sync finished!");
            Toast.makeText(getApplicationContext(), "Sync Finished",
                    Toast.LENGTH_SHORT).show();
        }
    };
    private BroadcastReceiver syncStaredReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "Sync started!");
            Toast.makeText(getApplicationContext(), "Sync started...",
                    Toast.LENGTH_SHORT).show();
        }
    };

}
